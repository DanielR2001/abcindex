﻿namespace ABCindex
{
    public interface IIndexTool
    {
        void GenerateAbcIndex(string title);
        void GenerateAbcSetList(string title);
        void OutputNewAbcIndexFile();
        void OutputNewFileWithIndexAppended();
        void OutputSimpleToConsole();
        void ReadInput(string InputFilename);
        void SortListAlpha();
    }
}