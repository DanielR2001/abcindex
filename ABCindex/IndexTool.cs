﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;

namespace ABCindex
{
    public class IndexTool : IIndexTool
    {
        private string inputFilename;
        private readonly IFileSystem fileSystem;
        private string[] inputFileLines;
        public List<Song> songList = new();
        public List<string> newAbcIndex = new();
        public Dictionary<int, List<string>> SongSets = new();


        const string colStart = @"%%multicol start";
        const string beginText = @"%%begintext";
        const string endText = @"%%endtext";
        const string colNew = @"%%multicol new";

        public IndexTool(IFileSystem fileSystem)
        {
            this.fileSystem = fileSystem;
        }

        public void ReadInput(string InputFilename)
        {
            inputFilename = InputFilename;
            string index = "";
            inputFileLines = fileSystem.File.ReadAllLines(inputFilename);
            foreach (string line in inputFileLines)
            {
                if (line.StartsWith("X:"))
                {
                    index = line.Substring(2).Trim();
                }
                if (line.StartsWith("T:"))
                {
                    string songTitle = line.Substring(2).Trim();
                    songList.Add(new Song { SetNumber = index, Title = songTitle });

                    var n = int.Parse(index);
                    if (SongSets.ContainsKey(n))
                    {
                        SongSets[n].Add(songTitle);
                    }
                    else
                    {
                        SongSets[n] = new List<string> { songTitle };
                    }
                }
            }
        }

        public void SortListAlpha()
        {
            songList = songList.OrderBy(item => item.Title).ToList();
        }


        public void OutputSimpleToConsole()
        {
            songList.ForEach(item =>
            {
                Console.WriteLine(item.SetNumber + "\t" + item.Title);
            });
        }

        public void OutputNewAbcIndexFile()
        {
            fileSystem.File.WriteAllLines("abcIndex.abc", newAbcIndex);
            Console.WriteLine($"new file > abcIndex.abc");
        }

        public void OutputNewFileWithIndexAppended()
        {
            string file = Path.GetFileNameWithoutExtension(inputFilename);
            string NewPath = inputFilename.Replace(file, file + " (+index)");

            fileSystem.File.WriteAllLines(NewPath, inputFileLines);
            fileSystem.File.AppendAllLines(NewPath, newAbcIndex);
            Console.WriteLine($"new file > {NewPath}");
        }

        public void GenerateAbcIndex(string title)
        {
            var linesPerPage = 35;

            newAbcIndex.Add(@$"X:999
%%titleformat  T  
T:{title}
M:4/4
L:1/4
K:c
I:scale 0.9
x
%%pagewidth 8.25in
%%rightmargin 0.2in");

            var page = 1;
            var pageLine = 0;
            var titleCol = new List<string>();
            var setNumberCol = new List<string>();


            var col = 0;
            songList.ForEach(item =>
            {
                string shortTitle = IndexTool.shortTitle(item.Title);
                titleCol.Add(shortTitle);
                setNumberCol.Add(item.SetNumber);

                pageLine++;
                if (pageLine > linesPerPage)
                {
                    page = copyToDoc(newAbcIndex, page, titleCol, setNumberCol, col);
                    //switch col ? yes I made it col 0 or 2 - stupid eh?
                    col = col == 0 ? 2 : 0;

                    titleCol.Clear();
                    setNumberCol.Clear();
                    pageLine = 0;

                }
            });

            copyToDoc(newAbcIndex, page, titleCol, setNumberCol, col);

            if (col == 0) //might need to end this if last bit ends on col 0
            {
                newAbcIndex.Add("%%multicol end");

            }
        }

        private static string shortTitle(string item, int limit = 30)
        {
            return item.Length > limit ? item.Substring(0, limit) + "..." : item;
        }

        private int copyToDoc(List<string> doc, int page, List<string> titleCol, List<string> setNumberCol, int col)
        {
            var leftmarginCol = new string[] { "0.5in", "3.5in", "4in", "7in" };




            if ((col == 0) && (page > 1)) doc.Add("%%newpage");
            doc.Add(col == 0 ? colStart : colNew);
            doc.Add($"%%leftmargin {leftmarginCol[col]}");

            doc.Add(beginText);
            doc.AddRange(titleCol);
            doc.Add(endText);

            doc.Add(colNew);
            doc.Add($"%%leftmargin {leftmarginCol[col + 1]}");

            doc.Add(beginText);
            doc.AddRange(setNumberCol);
            doc.Add(endText);

            if (col == 2)
            {
                doc.Add("%%multicol end");
                page++;
            }

            return page;
        }



        public void GenerateAbcSetList(string title)
        {
            var linesPerPage = 35;

            newAbcIndex.Add(@$"X:999
%%titleformat  T  
T:{title}
M:4/4
L:1/4
K:c
I:scale 0.9
x
%%pagewidth 8.25in
%%multicol start
%%leftmargin 0.5in
%%begintext");

            var page = 1;
            var pageLine = 0;
            var suppressThe = true;

            foreach (var song in SongSets)
            {
                var output = song.Key + "     ";
                var first = true;
                //if line too long, output over >1 lines
                song.Value.ForEach(x =>
                {
                    if (output.Length > 75)
                    {
                        newAbcIndex.Add(output);
                        output = "          ";
                        pageLine++;
                    }
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        output += "  --  ";
                    }

                    var title = suppressThe ? x.Replace(", The", "") : x;
                    output += title;

                });

                //var setTitle = String.Join(" - ", song.Value);
                newAbcIndex.Add(output);
                pageLine++;


                if (pageLine > linesPerPage)
                {
                    newAbcIndex.Add(endText);
                    newAbcIndex.Add(@"%%multicol end");
                    newAbcIndex.Add(@"%%newpage");
                    newAbcIndex.Add("%%multicol start");
                    newAbcIndex.Add("%%begintext");
                    pageLine = 0;
                }
            }

            newAbcIndex.Add(endText);
        }
    }
}
