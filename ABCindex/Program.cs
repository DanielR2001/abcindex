﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;

namespace ABCindex
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Count() == 0)
            {
                Console.WriteLine("Usage ABCIndex <mymusic.abc>");
                Console.WriteLine(" no flag  output to console");
                Console.WriteLine("  --abc   produce index.abc file with columns");
                Console.WriteLine("  --end   output <original>+index.abc with index at end");
                Console.WriteLine("  --sets  include tunes in set order");
                return;
            }


            var serviceProvider = new ServiceCollection()
              .AddSingleton<IIndexTool, IndexTool>()
              .AddSingleton<IFileSystem, FileSystem>()
              .BuildServiceProvider();

   
            string inputFileName = args[0];
            Console.WriteLine(inputFileName);

            if (!File.Exists(inputFileName))
            {
                Console.WriteLine("does not exist");
                return;
            }

            var indexer = serviceProvider.GetService<IIndexTool>();
            indexer.ReadInput(inputFileName);
            var makeAbcIndex = args.Any(x => x.Equals("--abc"));
            var appendToEnd = args.Any(x => x.Equals("--end"));
            var includeSetlist = args.Any(x => x.Equals("--sets"));
            if (makeAbcIndex || appendToEnd || includeSetlist)
            {
                //if (includeSetlist) indexer.GenerateAbcIndex("SETS");
                indexer.SortListAlpha();
                indexer.GenerateAbcIndex("INDEX");
                if (includeSetlist) indexer.GenerateAbcSetList("SETS");

                if (makeAbcIndex) indexer.OutputNewAbcIndexFile();

                if (appendToEnd) indexer.OutputNewFileWithIndexAppended();
                return;
            }

            indexer.OutputSimpleToConsole();

        }
    }
}
