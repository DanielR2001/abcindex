
using ABCindex;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO.Abstractions.TestingHelpers;

namespace Indexer.Tests
{
    [TestClass]
    public class IndexerTests
    {
        private IndexTool sut;

        [TestInitialize]
        public void start()
        {
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { @"tunes", new MockFileData("X:1\nT:song 1\nT:song 2\nX:2\nT:song 3\nT:song 4") },
            });
          
            sut = new IndexTool(fileSystem);

            sut.ReadInput(@"tunes");
        }

        [TestMethod]
        public void CanGenerateIndex()
        {
            sut.GenerateAbcIndex("test tunes");
            Assert.IsNotNull(sut.newAbcIndex);
            Assert.IsTrue(sut.newAbcIndex.Count > 0);

            //has right title
            Assert.IsTrue(sut.newAbcIndex[0].Contains("T:test tunes"));

            //songs are listed somewhere
            Assert.IsTrue(sut.newAbcIndex.Exists(x => x.Contains("song 4")));
        }

        [TestMethod]
        public void CanGenerateSet()
        {
            sut.GenerateAbcSetList("set");
            Assert.IsNotNull(sut.newAbcIndex);
            Assert.IsTrue(sut.newAbcIndex.Count > 0);

            //has right title
            Assert.IsTrue(sut.newAbcIndex[0].Contains("T:set"));

            //songs are listed somewhere         
            Assert.IsTrue(sut.newAbcIndex.Exists(x => x.Contains("song 1  --  song 2")));
        }

    }
}
